																			/* ### BIBLIOS ### */

#include<stdio.h>
#include<stdlib.h>
#include<conio.h>
#include<string.h>
#include<windows.h>
																			/* ###  GLOBS  ### */

char FIELD[3][3] = { 0 }, LINES[3] = { 0 }/* | - + */, SIGNS[2] = { 0 }/* 1. Zeichen: Gegner; 2. Zeichen: Spieler */, Clear_Field[3][3] = { 0 }, opSIGNS = 'X';
int WIN = 0, LOSE = 0, DRAW = 0, ROUND = 0, opFIELD = 0, opLINES = 0, opMODE = 0;
																			/* ### PROTOS  ### */

																	/* ### Mainloops ### */
void menuloop();
void gameloop();
																	/* ### Menufuncs ### */
int  Menu_Output(const char str1[20], const char str2[20], const char str3[20], const char str4[20], const char str5[20], const char title[25]);
void Start_Game();
void Options();
void Stats();
void Creds();
void Easter_Egg();
																	/* ### Gamefuncs ### */
void Field_Output();
void Def_Globs();
int Your_Input();
int CPU_Input(int);
void Look4Win(char);
																			/* ###  MAIN   ### */

int main() {
	menuloop();
	return 0;
}
																			/* ###  FUNKS  ### */

																	/* ### Mainloops ### */
void menuloop() {
	system("cls");
	switch (Menu_Output("Start", "Options", "Stats", "Credits", "Exit", "Bulletproofed TicTacToe")) {
	case 0:
		Start_Game();
		break;
	case 1:
		Options();
		break;
	case 2:
		Stats();
		break;
	case 3:
		Creds();
		break;
	case 4:
		exit(0);
		break;
	}
}
void gameloop() {
	int chose;
	ROUND = 0;
	Def_Globs();
	Field_Output();
	while (1) {
		ROUND++;
		chose = Your_Input();
		Look4Win(SIGNS[1]);
		CPU_Input(chose);
		Look4Win(SIGNS[0]);
		Field_Output();
	}
}
																	/* ### Menufuncs ### */
int  Menu_Output(const char str1[20], const char str2[20], const char str3[20], const char str4[20], const char str5[20], const char title[25]) {
	static int row = 0;
	int chose, i, j = strlen(title);
	char selec[5] = { ' ', ' ', ' ', ' ', ' ' };
	while (1) {
		//TITLE
		system("cls");
		printf("    %s\n    ", title);
		for (i = 0; i<j; i++) {
			printf("%c", 205);
		}
		printf("\n\n");
		//MENU
		//Übertrag
		row<0 ? row = 4 : row;
		row>4 ? row = 0 : row;
		//Set
		selec[row] = '>';
		//Output
		printf(" %c %s\n", selec[0], str1);
		printf(" %c %s\n", selec[1], str2);
		printf(" %c %s\n", selec[2], str3);
		printf(" %c %s\n", selec[3], str4);
		printf(" %c %s\n", selec[4], str5);
		//Input
		chose = getch() - 48;
		//Processing
		switch (chose) {
		case 8:
			selec[row] = ' ';
			row--;
			break;
		case 2:
			selec[row] = ' ';
			row++;
			break;
		case 5:
			return row;
			break;
		}
	}
}
void Start_Game() {
	system("cls");
	printf("loading");
	int i;
	for (i = 0; i<3; i++) {
		printf(".");
		Sleep(500);
	}
	Sleep(500);
	gameloop();
}
void Options() {
	system("cls");
	static char sfield[20] = "Field:      NumPad", slines[20] = "Lines:      Single", ssigns[20] = "Sign:       X", smode[20] = "DevMode:    Off";
	while (1) {
		switch (Menu_Output(sfield, slines, ssigns, smode, "Back to Menu", "Options")) {
		case 0:
			opFIELD = (opFIELD + 1) % 2;
			if (opFIELD) {
				strcpy(sfield, "Field:      Normal");
			}
			else {
				strcpy(sfield, "Field:      NumPad");
			}
			break;
		case 1:
			opLINES = (opLINES + 1) % 2;
			if (opLINES) {
				strcpy(slines, "Lines:      Double");
			}
			else {
				strcpy(slines, "Lines:      Single");
			}
			break;
		case 2:
			system("cls");
			strcpy(ssigns, "[press a key]");
			printf("    Options\n    %c%c%c%c%c%c%c\n\n", 205, 205, 205, 205, 205, 205, 205);
			printf("   %s\n", sfield);
			printf("   %s\n", slines);
			printf("   %s\n", ssigns);
			printf("   %s\n", smode);
			printf("   Back to Menu\n");
			opSIGNS = getch();
			strcpy(ssigns, "Sign:       ");
			ssigns[12] = opSIGNS;
			break;
		case 3:
			opMODE = (opMODE + 1) % 2;
			if (opMODE) {
				strcpy(smode, "DevMode:    On");
			}
			else {
				strcpy(smode, "DevMode:    Off");
			}
			break;
		case 4:
			menuloop();
			break;
		}
	}
}
void Stats() {
	char swins[20] = { 'W', 'i', 'n', 's', ':', ' ', ' ', ' ', (char) (WIN / 10 + 48), (char) (WIN - (WIN / 10) * 10 + 48) }, sloses[20] = { 'L', 'o', 's', 'e', 's', ':', ' ', ' ', (char) (LOSE / 10 + 48), (char) (LOSE - (LOSE / 10) * 10 + 48) }, sdraws[20] = { 'D', 'r', 'a', 'w', 's', ':', ' ', ' ', (char) (DRAW / 10 + 48), (char) (DRAW - (DRAW / 10) * 10 + 48) };
	switch (Menu_Output(swins, sloses, sdraws, "Reset all", "Back to Menu", "Stats")) {
	case 0:
	case 1:
	case 2:
		Stats();
		break;
	case 3:
		WIN = 0;
		LOSE = 0;
		DRAW = 0;
		Stats();
		break;
	case 4:
		menuloop();
		break;
	}
}
void Creds() {
	system("cls");
	printf("    Credits\n");
	printf("    %c%c%c%c%c%c%c\n\n", 205, 205, 205, 205, 205, 205, 205);
	printf("              Developer   C. Schmidtmeier     \n");
	printf("                   Size   308,68 KiB          \n");
	printf("             DevMode is   useless             \n");
	printf("            don\'t press   \\                   \n\n");
	printf("              TicTacToe (c)  8.1.3            \n");
	printf("      Copyright 2016 Nicht_Schmitdmayr Ltd.   \n");
	printf("              All Rights Reserved.            \n\n");
	printf("  > Back to Menu\n");
	if (getch() == '\\') {
		Easter_Egg();
	}
	menuloop();
}
void Easter_Egg() {
	system("cls");
	printf("832331985665");
	getch();
}
																	/* ### Gamefuncs ### */
void Field_Output() {
	int i, j;
	system("cls");
	for (i = 0; i<3; i++) {
		for (j = 0; j<3; j++) {
			switch (FIELD[i][j]) {
			case ':':
				printf("[");
				break;
			case ';':
				printf("(");
				break;
			case ')':
				printf(";");
				break;
			case ']':
				printf(":");
				break;
			default:
				printf(" ");
			}
			printf("%c", FIELD[i][j]);
			if (j<2) {
				printf("%c", LINES[0]);
			}
			else {
				printf("\n");
			}
		}
		if (i<2) printf("%c%c%c%c%c%c%c%c\n", LINES[1], LINES[1], LINES[2], LINES[1], LINES[1], LINES[2], LINES[1], LINES[1]);
	}
}
void Def_Globs() {
	int i, j;
	//Field
	char NumPad[3][3] = { '7', '8', '9',
		'4', '5', '6',
		'1', '2', '3' };
	char Normal[3][3] = { '1', '2', '3',
		'4', '5', '6',
		'7', '8', '9' };
	if (opFIELD) {
		for (i = 0; i<3; i++) {
			for (j = 0; j<3; j++) {
				FIELD[i][j] = Normal[i][j];
				Clear_Field[i][j] = Normal[i][j];
			}
		}
	}
	else {
		for (i = 0; i<3; i++) {
			for (j = 0; j<3; j++) {
				FIELD[i][j] = NumPad[i][j];
				Clear_Field[i][j] = NumPad[i][j];
			}
		}
	}
	//Lines
	char Single[3] = { (char) 179, (char) 196, (char) 197 };
	char Double[3] = { (char) 186, (char) 205, (char) 206 };
	if (opLINES) {
		for (i = 0; i<3; i++) {
			LINES[i] = Double[i];
		}
	}
	else {
		for (i = 0; i<3; i++) {
			LINES[i] = Single[i];
		}
	}
	//Char
	if (opSIGNS == 'O') {
		SIGNS[0] = 'X';
	}
	else {
		SIGNS[0] = 'O';
	}
	SIGNS[1] = opSIGNS;
}
int Your_Input() {
	int chose = getch() - 49;
	if (!opFIELD&&chose>5) {
		chose -= 6;
	}
	else {
		if (!opFIELD&&chose<3) {
			chose += 6;
		}
	}
	if (chose<0 || chose>8 || FIELD[chose / 3][chose % 3]<Clear_Field[chose / 3][chose % 3] || FIELD[chose / 3][chose % 3]>Clear_Field[chose / 3][chose % 3]) {
		printf("ERROR\a\n");
		return Your_Input();
	}
	else {
		FIELD[chose / 3][chose % 3] = SIGNS[1];
		return 10 * (chose / 3) + (chose % 3);
	}
}
int CPU_Input(int enemy_input) {
	int i = 0, j = 0;
	char k = 0;
	//Verhindern/Fertig machen
	for (i = 0; i<2; i++) {
		//Ecken
		//Feld 00
		if (((FIELD[0][1] == SIGNS[i] && FIELD[0][2] == SIGNS[i]) || (FIELD[1][0] == SIGNS[i] && FIELD[2][0] == SIGNS[i]) || (FIELD[1][1] == SIGNS[i] && FIELD[2][2] == SIGNS[i])) && FIELD[0][0] == Clear_Field[0][0]) {
			FIELD[0][0] = SIGNS[0];
			return 0;
		}
		//Feld 02
		if (((FIELD[0][0] == SIGNS[i] && FIELD[0][1] == SIGNS[i]) || (FIELD[1][2] == SIGNS[i] && FIELD[2][2] == SIGNS[i]) || (FIELD[1][1] == SIGNS[i] && FIELD[2][0] == SIGNS[i])) && FIELD[0][2] == Clear_Field[0][2]) {
			FIELD[0][2] = SIGNS[0];
			return 0;
		}
		//Feld 20
		if (((FIELD[0][0] == SIGNS[i] && FIELD[1][0] == SIGNS[i]) || (FIELD[2][1] == SIGNS[i] && FIELD[2][2] == SIGNS[i]) || (FIELD[0][2] == SIGNS[i] && FIELD[1][1] == SIGNS[i])) && FIELD[2][0] == Clear_Field[2][0]) {
			FIELD[2][0] = SIGNS[0];
			return 0;
		}
		//Feld 22
		if (((FIELD[0][2] == SIGNS[i] && FIELD[1][2] == SIGNS[i]) || (FIELD[2][0] == SIGNS[i] && FIELD[2][1] == SIGNS[i]) || (FIELD[1][1] == SIGNS[i] && FIELD[0][0] == SIGNS[i])) && FIELD[2][2] == Clear_Field[2][2]) {
			FIELD[2][2] = SIGNS[0];
			return 0;
		}
		//Kanten
		//Feld 01
		if (((FIELD[0][0] == SIGNS[i] && FIELD[0][2] == SIGNS[i]) || (FIELD[1][1] == SIGNS[i] && FIELD[2][1] == SIGNS[i])) && FIELD[0][1] == Clear_Field[0][1]) {
			FIELD[0][1] = SIGNS[0];
			return 0;
		}
		//Feld 10
		if (((FIELD[0][0] == SIGNS[i] && FIELD[2][0] == SIGNS[i]) || (FIELD[1][1] == SIGNS[i] && FIELD[1][2] == SIGNS[i])) && FIELD[1][0] == Clear_Field[1][0]) {
			FIELD[1][0] = SIGNS[0];
			return 0;
		}
		//Feld 12
		if (((FIELD[0][2] == SIGNS[i] && FIELD[2][2] == SIGNS[i]) || (FIELD[1][1] == SIGNS[i] && FIELD[1][0] == SIGNS[i])) && FIELD[1][2] == Clear_Field[1][2]) {
			FIELD[1][2] = SIGNS[0];
			return 0;
		}
		//Feld 21
		if (((FIELD[0][1] == SIGNS[i] && FIELD[1][1] == SIGNS[i]) || (FIELD[2][2] == SIGNS[i] && FIELD[2][0] == SIGNS[i])) && FIELD[2][1] == Clear_Field[2][1]) {
			FIELD[2][1] = SIGNS[0];
			return 0;
		}
		//Mitte
		//Feld 11
		if (((FIELD[0][1] == SIGNS[i] && FIELD[2][1] == SIGNS[i]) || (FIELD[1][0] == SIGNS[i] && FIELD[1][2] == SIGNS[i]) || (FIELD[0][0] == SIGNS[i] && FIELD[2][2] == SIGNS[i]) || (FIELD[0][2] == SIGNS[i] && FIELD[2][0] == SIGNS[i])) && FIELD[1][1] == Clear_Field[1][1]) {
			FIELD[1][1] = SIGNS[0];
			return 0;
		}
	}
	//Mitte besetzen
	if (FIELD[1][1] == Clear_Field[1][1]) {
		FIELD[1][1] = SIGNS[0];
		return 0;
	}
											/*#################################*/
											/*#   Anfang Veränderbarer Teil   #*/
											/*#################################*/
	//Ecken
	if (enemy_input == 0 || enemy_input == 2 || enemy_input == 6 || enemy_input == 8) {

	}
	//Kanten
	if (enemy_input == 1 || enemy_input == 3 || enemy_input == 5 || enemy_input == 7) {

	}
	//Mitte
	if (enemy_input == 4) {

	}
											/*#################################*/
											/*#   Ende   Veränderbarer Teil   #*/
											/*#################################*/
	//Notfall
	for (i = 0; i<3; i++) {
		for (j = 0; j<3; j++) {
			if (FIELD[i][j] == Clear_Field[i][j]) {
				FIELD[i][j] = SIGNS[0];
				return 0;
			}
		}
	}
	return 0;
}
void Look4Win(char needle) {
	int i, j, a = 0;
	for (i = 0; i<3; i++) {
		if ((FIELD[i][0] == FIELD[i][1] && FIELD[i][1] == FIELD[i][2]) ||
			(FIELD[0][i] == FIELD[1][i] && FIELD[1][i] == FIELD[2][i]) ||
			(FIELD[0][0] == FIELD[1][1] && FIELD[1][1] == FIELD[2][2]) ||
			(FIELD[0][2] == FIELD[1][1] && FIELD[1][1] == FIELD[2][0]))
		{
			if (needle == SIGNS[1]) {
				Field_Output();
				WIN++;
				Sleep(1000);
				while (1) {
					switch (Menu_Output("New Game", "Stats", "Options", "Back to Menu", "Exit", "You won!")) {
					case 0:
						gameloop();
						break;
					case 1:
						Stats();
						break;
					case 2:
						Options();
						break;
					case 3:
						menuloop();
						break;
					case 4:
						exit(0);
						break;
					}
				}
				break;
			}
			if (needle == SIGNS[0]) {
				Field_Output();
				LOSE++;
				Sleep(1000);
				while (1) {
					switch (Menu_Output("New Game", "Stats", "Options", "Back to Menu", "Exit", "You lost!")) {
					case 0:
						gameloop();
						break;
					case 1:
						Stats();
						break;
					case 2:
						Options();
						break;
					case 3:
						menuloop();
						break;
					case 4:
						exit(0);
						break;
					}
				}
				break;
			}
		}
		for (j = 0; j<3; j++) {
			if (FIELD[i][j] != Clear_Field[i][j]) {
				a++;
			}
		}
	}
	if (a == 9) {
		Field_Output();
		DRAW++;
		Sleep(1000);
		while (1) {
			switch (Menu_Output("New Game", "Stats", "Options", "Back to Menu", "Exit", "Draw!")) {
			case 0:
				gameloop();
				break;
			case 1:
				Stats();
				break;
			case 2:
				Options();
				break;
			case 3:
				menuloop();
				break;
			case 4:
				exit(0);
				break;
			}
		}
	}
}
