#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>


#define SIZE 3

#define NEXT(t) (((t) == PLAYER) ? COM : ((t) == COM) ? PLAYER : EMPTY)

#define COM_CHAR    'O'
#define PLAYER_CHAR 'X'

#define MIN(x, y) ((x) < (y) ? (x) : (y))
#define MAX(x, y) ((x) > (y) ? (x) : (y))
#define ABS(x)    ((x) > 0 ? (x) : (-x))

#define VERT  ((char) 179)
#define HORI  ((char) 196)
#define CROSS ((char) 197)


typedef enum symbol {
	COM = -1,
	EMPTY = 0,
	PLAYER = 1
} symbol;

typedef struct opts_t {
	symbol starter;
	bool helper;
	enum { ASCENDING, NUMPAD } field_variant;
} opts_t;


void print_usage(int argc, char *argv[]);
void print_field(symbol field[SIZE][SIZE], opts_t* opts);
symbol check_win(symbol field[SIZE][SIZE]);
bool check_full_field(symbol field[SIZE][SIZE]);
void get_player_move(symbol field[SIZE][SIZE], opts_t* opts);
void make_best_move(symbol field[SIZE][SIZE], symbol turn);
symbol minimax(symbol field[SIZE][SIZE], symbol turn, int lower, int upper, int* best_i, int* best_j);


int main(int argc, char *argv[]) {
	// parse args
	opts_t opts = { .field_variant = ASCENDING, .starter = PLAYER, .helper = false };
	for (int i = 1; i != argc; ++i) {
		if (strcmp(argv[i - 1], "-f") == 0 || strcmp(argv[i - 1], "--field") == 0) {
			switch (argv[i][0]) {
			case 'a':
				opts.field_variant = ASCENDING;
				break;
			case 'n':
				opts.field_variant = NUMPAD;
				break;
			default:
				print_usage(argc, argv);
				break;
			}
		}
		else if (strcmp(argv[i - 1], "-s") == 0 || strcmp(argv[i - 1], "--starter") == 0) {
			switch (argv[i][0]) {
			case 'c':
				opts.starter = COM;
				break;
			case 'p':
				opts.starter = PLAYER;
				break;
			default:
				print_usage(argc, argv);
				break;
			}
		}
		else if (strcmp(argv[i], "--com-help") == 0) {
			opts.helper = true;
		}
	}

	// init field
	symbol field[SIZE][SIZE] = { 0 };
	memset(field, 0, sizeof(field));

	// game loop
	symbol turn = opts.starter;
	while (check_win(field) == EMPTY && !check_full_field(field)) {
		if (turn == PLAYER) {
			// print field
			print_field(field, &opts);

			// get player input
			get_player_move(field, &opts);
			printf("\n");
		}
		else if (turn == COM) {
			// find best move
			make_best_move(field, COM);
		}
		turn = NEXT(turn);
	}

	// print result
	print_field(field, &opts);
	switch (check_win(field)) {
	case PLAYER:
		printf("You win\n");
		break;
	case EMPTY:
		printf("It's a draw\n");
		break;
	case COM:
		printf("You lose\n");
		break;
	}

	return EXIT_SUCCESS;
}

void print_usage(int argc, char *argv[]) {
	fprintf(stderr, "Usage: %s [-f | --field FIELD] [-s | --starter STARTER] [--com-help]\n", argv[0]);
	exit(EXIT_FAILURE);
}

void print_field(symbol field[SIZE][SIZE], opts_t* opts) {
	for (int i = 0; i != SIZE; ++i) {
		for (int j = 0; j != SIZE; ++j) {
			// get player icon (or placeholder)
			switch (field[i][j]) {
			case COM:
				printf(" %c", COM_CHAR);
				break;
			case PLAYER:
				printf(" %c", PLAYER_CHAR);
				break;
			case EMPTY:
				// check for numpad mode
				if (opts->field_variant == ASCENDING)
					printf("%2d", i * SIZE + j + 1);
				else if (opts->field_variant == NUMPAD)
					printf("%2d", (SIZE - i - 1) * SIZE + j + 1);
				break;
			}
			
			// print column seperator (or newline)
			printf("%c", (j < SIZE - 1) ? VERT : '\n');
		}

		// print row seperator
		if (i < SIZE - 1) {
			for (int j = 0; j != SIZE; ++j) {
				printf("%c%c", HORI, HORI);
				printf("%c", (j < SIZE - 1) ? CROSS : '\n');
			}
		}
	}
}

symbol check_win(symbol field[SIZE][SIZE]) {
	int vert, hori, diag;

	// iterate over rows and cols
	for (int i = 0; i != SIZE; ++i) {
		// add up the ith row/col
		vert = hori = 0;
		for (int j = 0; j != SIZE; ++j) {
			vert += field[i][j];
			hori += field[j][i];
		}

		// check if its full of the same value
		if (ABS(vert) == SIZE)
			return field[i][0];
		if (ABS(hori) == SIZE)
			return field[0][i];
	}

	// iterate over diagonals
	for (int i = 0; i != 2; ++i) {
		diag = 0;
		// add up the diagonal
		for (int j = 0; j != SIZE; ++j)
			diag += field[j][(i == 0) ? (SIZE - j - 1) : j];

		// check if its full of the same value
		if (ABS(diag) == SIZE)
			return field[0][(i == 0) ? (SIZE - 1) : 0];
	}

	// no winner
	return EMPTY;
}

bool check_full_field(symbol field[SIZE][SIZE]) {
	for (int i = 0; i != SIZE; ++i)
		for (int j = 0; j != SIZE; ++j)
			if (field[i][j] == EMPTY)
				return false;
	return true;
}

void get_player_move(symbol field[SIZE][SIZE], opts_t* opts) {
	int x, i, j;
	while (true) {
		// read character
		x = getchar();

		// check for help
		if ((x == '\n' || x == 'h') && opts->helper) {
			make_best_move(field, PLAYER);
			break;
		}

		// catch the newline (if not caught already)
		if (x != '\n')
			getchar();

		// range check
		x -= ('0' + 1);  // maps '1' -> 0; ...; '9' -> 8
		if (x < 0 || x > 8) {
			fprintf(stderr, "Invalid character\n");
			continue;
		}

		// get coords
		i = x / SIZE;
		j = x % SIZE;

		// wrap if in numpad mode
		if (opts->field_variant == NUMPAD)
			i = SIZE - i - 1;

		// empty check
		if (field[i][j] != EMPTY) {
			fprintf(stderr, "Spot is not empty\n");
			continue;
		}

		// all checks passed
		field[i][j] = PLAYER;
		break;
	}
}

 void make_best_move(symbol field[SIZE][SIZE], symbol turn) {
	int i, j;
	minimax(field, turn, COM - 1, PLAYER + 1, &i, &j);
	field[i][j] = turn;
}

symbol minimax(symbol field[SIZE][SIZE], symbol turn, int lower, int upper, int* best_i, int* best_j) {
	// terminate if either someone has won or the field is full
	int winner;
	if ((winner = check_win(field)) != EMPTY || check_full_field(field))
		return winner;

	// iterate over all spots
	symbol score, best_score = (turn == PLAYER) ? COM - 1 : PLAYER + 1;
	for (int i = 0; i != SIZE; ++i) {
		for (int j = 0; j != SIZE; ++j) {
			// skip if not empty
			if (field[i][j] != EMPTY)
				continue;

			// fill the current spot and update bounds
			field[i][j] = turn;
			lower = (turn == PLAYER) ? best_score : lower;
			upper = (turn != PLAYER) ? best_score : upper;

			// minimax again for the other perspective
			score = minimax(field, NEXT(turn), lower, upper, NULL, NULL);

			// clear spot previously filled
			field[i][j] = EMPTY;

			// keep the better score and check the boundaries
			best_score = (turn == PLAYER) ? MAX(best_score, score) : MIN(best_score, score);
			if (((turn != PLAYER) && best_score < lower) || ((turn == PLAYER) && best_score > upper))
				return best_score;

			// keep the position (if wanted)
			if (best_i != NULL && best_j != NULL && score == best_score) {
				*best_i = i;
				*best_j = j;
			}

		}
	}

	return best_score;
}
